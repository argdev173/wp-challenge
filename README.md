# WP-Challenge for Workana

by Gonzalo Bonini





## Instalación

1) Clona el [repositorio: https://argdev173@bitbucket.org/argdev173/wp-challenge.git ](https://argdev173@bitbucket.org/argdev173/wp-challenge.git)

2) El sitio utiliza un vhost, para configurarlo debes ir al directorio etc/host de tu sistema operativo e ingresar: **192.168.33.10 wpchallenge.test**

3) Abre la consola y haz un **cd** a la carpeta donde clonaste el repositorio, ejecuta **vagrant up**. ***provision-post.sh*** importa la base de datos **wordpress.sql**. La instalación puede tomar hasta cinco minutos. 

4) Puedes visitar el sitio en http://wpchallenge.test/



## El sitio

![](W:\wp-challenge\Docs\screenshot.jpg)



El sitio está basado en el tema Shapely, hace uso del plugin **Advanced Custom fields ** para los parametros numéricos en el Custom Post Type **Cuenta**.

La definición del tipo de entrada está en el functions.php del child-theme. En **primos.php** la implementación del ejercicio de algoritmia; esta misma función es usada en el front-end; implementado con shortcodes, AJAX y jQuery.

Puede que tengas que actualizar los **permalinks** en el panel antes de poder usar el widget de la calculadora. Alcanzar con ir a http://wpchallenge.test/wp-admin/options-permalink.php y presionar Guardar.



### Cuentas

En la vista single las cuentas muestran y suman los campos numéricos "Total" y "Comisión".


![](W:\wp-challenge\Docs\cuentas.jpg)



### Acceso al panel

usuario: **admin**

contraseña: **admin**





>Wp - Challenge
