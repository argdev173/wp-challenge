<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

function my_modify_main_query( $query ) {
  if ( $query->is_home() && $query->is_main_query() ) { // Run only on the homepage
  $query->query_vars['post_type'] = "cuenta"; // Exclude my featured category because I display that elsewhere
  }
}
// Hook my above function to the pre_get_posts action
add_action( 'pre_get_posts', 'my_modify_main_query' );

// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Cuentas', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Cuenta', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Cuentas', 'text_domain' ),
		'name_admin_bar'        => __( 'Cuenta', 'text_domain' )
	);
	$args = array(
		'label'                 => __( 'Cuenta', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page'
	);
	register_post_type( 'cuenta', $args );

}
add_action( 'init', 'custom_post_type', 0 );

include_once("primos.php");

add_action('wp_ajax_siguientePrimo', 'siguientePrimo');

function get_primos_calc($atts) {
    return '
    <div class="form-group">
    <label for="calc">Encuentra el numero primo proximo:</label>
    <input type="number" class="form-control" id="calc" placeholder="10">
    </div>
    <div class="form-group">
    <button type="button" id="calcular" class="btn btn-primary">Calcular</button>
    </div>
    <div class="alert alert-success" role="alert">
      Ingresa un numero
    </div>
    <script>
    jQuery("#calcular").click(function(){
      var num = jQuery("#calc").val();
      jQuery(".alert").html("Cargando..");
      jQuery.ajax({
       url: "/wp-admin/admin-ajax.php",
       data: {"action": "siguientePrimo", "num": num},
       type: "POST",
       success: function(output) {
                    jQuery(".alert").html(output);
                    console.log(output);
                }
        });
    });
    </script>
';
}

add_shortcode('primos_calc', 'get_primos_calc');

?>
