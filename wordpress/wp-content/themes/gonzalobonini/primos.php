<?php

function esPrimo($num) {
    $n = abs($num); // Valor absoluto
    $i = 2;
    if($n == 1 || $n == 0) // Cero y uno no son primos
      return false;
    while ($i <= sqrt($n)) {
        if ($n % $i == 0) { // Si la división es exacta, no es primo
            return false;
        }
        $i++;
    }
    return true;
    die();
}


function siguientePrimo() {
    $num = $_POST['num'];
    $primoSiguiente = $num+1;  // Evitamos devolver el mismo $num si este es primo
    while (!esPrimo($primoSiguiente)){
      $primoSiguiente++;
    }
    echo $primoSiguiente;
    die();
}

?>
